var express = require('express');
var router = express.Router();
var fs = require('fs');


/* main GET handler */
router.get('/', function(req, res) {

	var q = req.query;

	if(q.api_key === '1234567890') {

	  	if(q.module_name && /^[a-zA-Z0-9_-]{4,32}$/.test(q.module_name)) {

	  		var fileName = 'tmp-data/' + q.module_name + '_' + q.api_key + '.json';

	  		var data = [];
	  		if(fs.existsSync(fileName)) {
	  			data = require('../' + fileName);
	  		}

	  		res.statusCode = 200;
	  		res.setHeader('Content-Type', 'application/json');
	  		res.write(JSON.stringify(data));
	  		res.end();


	  	} else {

	  		// cannot get data without a valid module_name
	  		res.statusCode = 400;
	  		res.end();
	  	}

	  } else {
	  	// stop right there, invalid api key
	  	res.statusCode = 401;
	  	res.end();
	  }

});


/* main POST handler  */
// router.get('/post', function(req, res) {un instant
//   var post = req.query;
router.post('/', function(req, res) {
  var post = req.body;

  if(post.api_key === '1234567890') {

  	if(post.module_name && /^[a-zA-Z0-9_-]{4,32}$/.test(post.module_name)) {

  		var fileName = 'tmp-data/' + post.module_name + '_' + post.api_key + '.json';

  		var data = [];
  		if(fs.existsSync(fileName)) {
  			console.log(__dirname);
  			data = require('../' + fileName);
  		}

  		var jsoned_data = false;
  		try {
  			jsoned_data = JSON.parse(post.data);
  		} catch(ex) {

  		}

  		if(jsoned_data) {
    		data.push(jsoned_data);
	  		fs.writeFile(fileName, JSON.stringify(data), function(err) {
		  		res.statusCode = err ? 500 : 200;
		  		res.end();
	  		});

  		} else {
  			res.statusCode = 400;
	  		res.end();
  		}


  	} else {

  		// cannot post data without a valid module_name
  		res.statusCode = 400;
  		res.end();
  	}

  } else {
  	// stop right there, invalid api key
  	res.statusCode = 401;
  	res.end();
  }

});

module.exports = router;
