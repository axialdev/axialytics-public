

**CORE**
	POST / args
		api_key		string		REQUIRED	Identifies the caller				"1234567890"
		module_name	string		REQUIRED	Saves data for the right module		[a-zA-Z0-9_-]{4,32}
		data		JSON		REQUIRED	The data !
		
		
![Codeship Status for axialdev/Axialytics public](https://codeship.io/projects/c5d4a4c0-df83-0131-750e-0e2802c69076/status)](https://codeship.io/projects/24995)